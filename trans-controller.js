const Trans = require('./trans.js') 
const Barang = require('./barang.js')
const Corp = require('./corp.js')

class Transcontroller 
{
    async add(req,res)
    {
        try {
            if(req.auth){
                const item = await Barang.findByPk(req.body.Perusahaan)
                const corp = await Corp.findByPk(req.body.Barang)
                const total = parseFloat(req.body.Jumlah) * item.Harga
                const sisa = item.Stok - parseFloat(req.body.Jumlah)
                await Trans.create({
                     TglInput: req.body.Tanggal,
                     Corp: corp.Nama,
                     Item: item.Nama,
                     Qty: req.body.Jumlah,
                     Harga: item.Harga,
                     Total: total,
                     Sisa: sisa
                })
                res.status(200).json({message: 'success'})
            }
            else {
                throw new Error('whoops! user tidak dikenal')
            }
        } catch (e) {
            res.status(400).json(e.message)
        }
    }

    async data(req,res)
    {
        try {
            if(req.auth){
                const data = await Trans.findAll()
                res.status(200).json({message: 'success',data: data})
            } else {
                throw new Error('whoops! user tidak dikenal')
            }
        }
        catch (e) {
            res.status(400).json(e.message)
        }
    }

    async eksport(req,res)
    {
        try {
                const data = await Trans.findAll()
                let html = 'Tgl;Perusahaan;Barang;Jml;Harga;Total;Sisa Stok\n'
                const datum = JSON.parse(JSON.stringify(data))
                datum.forEach((e)=>{
                    html += (new Date(e.TglInput)).toLocaleDateString('id-Id') + ';'
                    html += e.Corp + ';'
                    html += e.Item + ';'
                    html += e.Qty + ';'
                    html += e.Harga + ';'
                    html += e.Total + ';'
                    html += e.Sisa + '\n'
                })
                res.set("Content-Type","text/csv");
                res.set("Content-Disposition","attachment; filename=transaction" + Date.now() + ".csv"); 
                res.send(html)
        }
        catch (e) {
            res.status(400).json(e.message)
        }
    }
}

module.exports = new Transcontroller()