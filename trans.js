const { DataTypes, Model } = require('sequelize')
const sequelize = require('./sequelize.js')

class Trans extends Model {}

Trans.init({
    TglInput: {
        type: DataTypes.STRING
    },
    Corp: {
        type: DataTypes.STRING
    },
    Item: {
        type: DataTypes.STRING
    },
    Qty: {
        type: DataTypes.DOUBLE
    },
    Harga: {
        type: DataTypes.DOUBLE
    },
    Total: {
        type: DataTypes.DOUBLE
    },
    Sisa: {
        type: DataTypes.DOUBLE
    }
},{
    sequelize,
    modelName: 'transaction'
})

module.exports = Trans